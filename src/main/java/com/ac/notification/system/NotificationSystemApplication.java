package com.ac.notification.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.time.ZoneId;
import java.util.TimeZone;

@SpringBootApplication
@EnableConfigurationProperties
public class NotificationSystemApplication {
  
  public static void main(String[] args) {
    SpringApplication.run(NotificationSystemApplication.class, args);
  }
  
}
