/*
 * Project:   <B>notification-system</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.factory;

import com.ac.notification.system.domain.NotificationEngine;
import com.ac.notification.system.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Component
public class NotificationServiceFactory {
  
  private final NotificationService[] services;
  
  public NotificationService createByEngine(NotificationEngine engine) {
  
    return Arrays.stream(services)
      .filter(e -> e.supports(engine))
      .findFirst()
      .orElseThrow(IllegalArgumentException::new);
  }
}
