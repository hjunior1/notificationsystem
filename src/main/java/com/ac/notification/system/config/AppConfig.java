package com.ac.notification.system.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
@EnableAutoConfiguration
@EnableScheduling
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class AppConfig {
  
  private final ConfigurationPrinter configurationPrinter;
  
  @PostConstruct
  public void printConfigurations() {
    
    configurationPrinter.getPrintableConfigurations().forEach(log::info);
  }
  
}
