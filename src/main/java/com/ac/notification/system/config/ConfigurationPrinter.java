package com.ac.notification.system.config;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

@Component
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class ConfigurationPrinter {
  
  private final Environment env;
  
  private final SmtpConfiguration smtp;
  
  @Value("${timezone}")
  private String timeZone;
  
  /**
   * Do NOT include passwords.
   */
  public List<String> getPrintableConfigurations() {
    List<String> info = new ArrayList<>();
    info.add("**************************************");
    info.add("Application started in............. ");
    includeActiveProfile(info);
    includeGeneralProperties(info);
    includeSmtpConfig(info);
    info.add("**************************************");
    return info;
  }
  
  private void includeActiveProfile(List<String> info) {
    info.add("Profiles ");
    String[] profiles = env.getActiveProfiles();
    if (profiles.length == 0) {
      info.add("  |-No active profiles set");
    } else {
      Arrays.asList(profiles).forEach(profile -> info.add("  |-Profile: " + profile));
    }
  }
  
  private void includeGeneralProperties(List<String> info) {
    info.add("General ");
    info.add("  |-Timezone set to: " + getApplicationDefaultTimeZone().getID());
  }
  
  private void includeSmtpConfig(List<String> info) {
    info.add("SMTP ");
    info.add("  |-Host: " + smtp.getHost());
    info.add("  |-Port: " + smtp.getPort());
    info.add("  |-Username: " + smtp.getUsername());
  }
  
  private TimeZone getApplicationDefaultTimeZone() {
  
    TimeZone.setDefault(TimeZone.getTimeZone(ZoneId.of(timeZone)));
    TimeZone applicationDefaultTimeZone = TimeZone.getTimeZone(ZoneId.of(timeZone));
    TimeZone.setDefault(applicationDefaultTimeZone);
    return applicationDefaultTimeZone;
  }
}
