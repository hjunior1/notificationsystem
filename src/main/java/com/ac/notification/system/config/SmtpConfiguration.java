/*
 * Project:   <B>notification-system</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.config;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
@ConfigurationProperties(prefix = "ac.engage.email.smtp")
@Getter
@Setter
@ToString
public class SmtpConfiguration {
  
  @Value("host")
  private String host;
  
  @Value("port")
  private String port;
  
  @Value("username")
  private String username;
  
  @Value("password")
  private String password;
  
  public Properties getProperties() {
  
    Properties properties = new Properties();
    properties.put("mail.smtp.host", host);
    properties.put("mail.smtp.port", port);
    properties.put("mail.smtp.starttls.enable", "true");
    properties.put("mail.smtp.auth", "true");
    
    return properties;
  }
}
