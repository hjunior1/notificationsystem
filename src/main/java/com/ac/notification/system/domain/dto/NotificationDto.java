/*
 * Project:   <B>notificationsystem</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.domain.dto;

import com.ac.notification.system.domain.NotificationEngine;
import com.ac.notification.system.domain.entity.Notification;
import com.ac.notification.system.domain.entity.Template;
import com.ac.notification.system.validation.EnumNotificationEngine;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class NotificationDto {
  
  @NotBlank(message = "Name is mandatory")
  private String sender;
  
  @NotBlank(message = "Recipient is mandatory")
  private String recipient;
  
  @NotBlank(message = "Subject is mandatory")
  private String subject;
  
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String message;
  
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private TemplateDto template;
  
  @EnumNotificationEngine
  private NotificationEngine engine;
  
  public static NotificationDto fromDomainEntity(final Notification notification) {
    return NotificationDto.builder()
        .message(notification.getMessage())
        .template(TemplateDto.fromDomainEntity(notification.getTemplate()))
        .sender(notification.getSender())
        .subject(notification.getSubject())
        .recipient(notification.getRecipient())
        .engine(notification.getEngine())
        .build();
  }
  
  public Notification toDomainEntity() {
  
    return Notification.builder()
        .message(this.message)
        .sender(this.sender)
        .subject(this.subject)
        .recipient(this.recipient)
        .engine(this.engine)
        .build();
  }
}
