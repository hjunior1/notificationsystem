/*
 * Project:   <B>notification-system</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.domain.dto;

import com.ac.notification.system.domain.entity.Template;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class TemplateDto {
  
  @NotBlank(message = "ID is mandatory")
  private Long id;
  
  @NotBlank(message = "Contents is mandatory")
  private String contents;
  
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String mediaType;
  
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Map<String, String> variables;
  
  public String toString() {
    return this.contents;
  }
  
  public static TemplateDto fromDomainEntity(final Template template) {
    
    return template != null ? TemplateDto.builder()
        .id(template.getId())
        .contents(template.getContents())
        .mediaType(template.getMediaType())
        .build() : null;
  }

}
