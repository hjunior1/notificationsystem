/*
 * Project:   <B>notification-system</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.domain.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import java.util.Map;

@Entity(name = "TEMPLATES")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Template {
  
  @Id
  @GeneratedValue
  private Long id;
  
  @NotBlank(message = "Message type is mandatory")
  private String mediaType;
  
  @NotBlank(message = "Contents is mandatory")
  private String contents;
  
  @Transient
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Map<String, String> variables;
  
  public String toString() {
    String expandedContents = contents;
    for (Map.Entry<String, String> entry : variables.entrySet()) {
      expandedContents = expandedContents.replaceAll("\\{" + entry.getKey() + "\\}", entry.getValue());
    }
    return expandedContents;
  }
}
