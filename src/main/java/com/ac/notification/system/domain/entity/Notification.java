/*
 * Project:   <B>notificationsystem</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.domain.entity;

import com.ac.notification.system.domain.NotificationEngine;
import com.ac.notification.system.validation.EnumNotificationEngine;
import com.sun.istack.Nullable;
import lombok.Builder;
import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;

@Data
@Builder
public class Notification {
  
  @Id
  @GeneratedValue
  private Long id;
  
  @NotBlank(message = "Name is mandatory")
  private String sender;
  
  @NotBlank(message = "Recipient is mandatory")
  private String recipient;
  
  @NotBlank(message = "Subject is mandatory")
  private String subject;
  
  @Nullable
  private String message;
  
  @Nullable
  private Template template;
  
  @EnumNotificationEngine
  private NotificationEngine engine;
}
