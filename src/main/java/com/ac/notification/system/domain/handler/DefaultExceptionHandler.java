package com.ac.notification.system.domain.handler;

import com.ac.notification.system.domain.dto.ErrorResponseDto;
import com.ac.notification.system.domain.exception.EmailException;
import com.ac.notification.system.domain.exception.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class DefaultExceptionHandler {

  @ExceptionHandler
  public ResponseEntity<ErrorResponseDto> handleUnknownExceptions(Exception ex) {
    ErrorResponseDto errorResponse = createErrorResponse(ex);
    log.error("Unexpected server error", ex);
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(EmailException.class)
  public ResponseEntity<ErrorResponseDto> handleEmailException(EmailException ex) {
    ErrorResponseDto errorResponse = createErrorResponse(ex);
    log.error("Unable to send email => {}", ex.getMessage(), ex);
    return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }
  
  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<ErrorResponseDto> handleIllegalArgumentsException(IllegalArgumentException ex) {
    ErrorResponseDto errorResponse = createErrorResponse(ex);
    log.error("Unable to process request => {}", ex.getMessage(), ex);
    return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
  }
  
  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<ErrorResponseDto> handleEntityNotFoundException(EntityNotFoundException ex) {
    ErrorResponseDto errorResponse = createErrorResponse(ex);
    log.error("Entity not found => {}", ex.getMessage(), ex);
    return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
  }
  
  private ErrorResponseDto createErrorResponse(Exception exception) {
    ErrorResponseDto.ErrorResponseDtoBuilder builder = ErrorResponseDto.builder();
    builder.error(exception.getMessage());
    if (log.isDebugEnabled()) {
      builder.callStacks(exception.getStackTrace());
    }
    return builder.build();
  }
}
