/*
 * Project:   <B>notificationsystem</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.controller;

import com.ac.notification.system.domain.NotificationEngine;
import com.ac.notification.system.domain.dto.TemplateDto;
import com.ac.notification.system.domain.entity.Template;
import com.ac.notification.system.factory.NotificationServiceFactory;
import com.ac.notification.system.domain.dto.NotificationDto;
import com.ac.notification.system.domain.entity.Notification;
import com.ac.notification.system.service.NotificationService;
import com.ac.notification.system.service.TemplateService;
import com.google.common.base.Preconditions;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Validated
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class NotificationController {
  
  private final NotificationServiceFactory serviceFactory;
  
  private final TemplateService templateService;
  
  @PostMapping(path = "/notify/{engine}", consumes = "application/json", produces = "application/json")
  public NotificationDto sendNotification(
      @RequestBody @Valid NotificationDto notificationDto,
      @PathVariable(name = "engine") String engineType) {
  
    Preconditions.checkArgument(notificationDto.getMessage() != null || notificationDto.getTemplate() != null);
    NotificationEngine engine = NotificationEngine.valueOf(engineType.toUpperCase());
    NotificationService notificationService = serviceFactory.createByEngine(engine);
    Notification notification = notificationDto.toDomainEntity();
    TemplateDto templateDto = notificationDto.getTemplate();
    // Replace actual template and variables
    if (templateDto != null) {
      Template template = templateService.getById(templateDto.getId());
      template.setVariables(templateDto.getVariables());
      template.setContents(template.toString());
      notification.setTemplate(template);
    }
    
    return notificationService.send(notification);
  }
}
