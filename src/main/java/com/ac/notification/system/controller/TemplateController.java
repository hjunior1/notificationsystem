/*
 * Project:   <B>notification-system</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.controller;

import com.ac.notification.system.domain.entity.Template;
import com.ac.notification.system.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@Validated
@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TemplateController {
  
  private final TemplateService service;
  
  @GetMapping(path = "/templates", produces = "application/json")
  public List<Template> getTemplates() {
    return service.getAll();
  }
  
  @GetMapping(path = "/templates/{id}", produces = "application/json")
  public Template getTemplates(@PathVariable Long id) {
    return service.getById(id);
  }
  
  @PostMapping(path = "/templates", consumes = "application/json", produces = "application/json")
  public Template postTemplate(@RequestBody @Valid Template template) {
    return service.save(template);
  }
  
  @DeleteMapping(path = "/templates/{id}")
  public void deleteTemplate(@PathVariable Long id) {
    service.delete(id);
  }
}
