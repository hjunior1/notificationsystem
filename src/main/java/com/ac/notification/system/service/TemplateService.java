/*
 * Project:   <B>notification-system</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.service;

import com.ac.notification.system.domain.entity.Template;
import com.ac.notification.system.domain.exception.EntityNotFoundException;
import com.ac.notification.system.repository.TemplateRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TemplateService {
  
  private final TemplateRepository repository;
  
  public List<Template> getAll() {
    return (List<Template>) repository.findAll();
  }
  
  public Template getById(Long id) {
    return repository
        .findById(id)
        .orElseThrow(() -> new EntityNotFoundException("Template", id));
  }
  
  public Template save(final Template template) {
    return repository.save(template);
  }
  
  public void delete(Long id) {
    repository.delete(
        repository
            .findById(id)
            .orElseThrow(() -> new EntityNotFoundException("Template", id)));
  }
}
