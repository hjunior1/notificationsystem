/*
 * Project:   <B>notification-system</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.service.smtp;

import com.ac.notification.system.config.SmtpConfiguration;
import com.ac.notification.system.domain.entity.Notification;
import com.ac.notification.system.domain.entity.Template;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.validation.Valid;

@Slf4j
@Validated
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class SmtpService {
  
  private final SmtpConfiguration smtpConfiguration;
  
  public void send(final @Valid Notification notification) throws MessagingException {
    Message mailMessage = new MimeMessage(createSession());
    mailMessage.setFrom(new InternetAddress(notification.getSender()));
    mailMessage.setRecipients(Message.RecipientType.TO, InternetAddress.parse(notification.getRecipient()));
    mailMessage.setSubject(notification.getSubject());
    
    MimeBodyPart mimeBodyPart = new MimeBodyPart();
    mimeBodyPart.setContent(getMessageByPriority(notification), getMessageType(notification));
    
    Multipart multipart = new MimeMultipart();
    multipart.addBodyPart(mimeBodyPart);
    mailMessage.setContent(multipart);
    
    Transport.send(mailMessage);
    log.info("Email notification successfully sent: {} => {}", notification, smtpConfiguration);
  }
  
  private Session createSession() {
    return Session.getInstance(smtpConfiguration.getProperties(), new Authenticator() {
      @Override
      protected PasswordAuthentication getPasswordAuthentication() {
        log.info("Authenticating to server smtpConfig={}", smtpConfiguration);
        return new PasswordAuthentication(smtpConfiguration.getUsername(), smtpConfiguration.getPassword());
      }
    });
  }
  
  private String getMessageByPriority(final Notification notification) {
    Template template = notification.getTemplate();
    if (template != null) {
      return template.toString();
    } else {
      return notification.getMessage();
    }
  }
  
  private String getMessageType(final Notification notification) {
    Template template = notification.getTemplate();
    if (template != null) {
      return template.getMediaType();
    } else {
      return MediaType.TEXT_PLAIN_VALUE;
    }
  }
}
