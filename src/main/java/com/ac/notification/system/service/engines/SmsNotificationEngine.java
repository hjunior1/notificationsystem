/*
 * Project:   <B>notification-system</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.service.engines;

import com.ac.notification.system.domain.NotificationEngine;
import com.ac.notification.system.domain.dto.NotificationDto;
import com.ac.notification.system.domain.entity.Notification;
import com.ac.notification.system.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class SmsNotificationEngine implements NotificationService {
  
  @Override
  public NotificationDto send(final Notification notification) {
    notification.setEngine(NotificationEngine.SMS);
    log.info("TODO Send actual sms: {}", notification);
    return NotificationDto.fromDomainEntity(notification);
  }
  
  @Override
  public boolean supports(final NotificationEngine engine) {
    return engine == NotificationEngine.SMS;
  }
}
