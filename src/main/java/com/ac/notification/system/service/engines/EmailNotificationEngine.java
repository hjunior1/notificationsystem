/*
 * Project:   <B>notificationsystem</B>
 * File:      <I>null.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  28/12/20
 * --------------------------------------------------------------------------
 * <I>Copyright 2020, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior.
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 */

package com.ac.notification.system.service.engines;

import com.ac.notification.system.config.SmtpConfiguration;
import com.ac.notification.system.domain.NotificationEngine;
import com.ac.notification.system.domain.dto.NotificationDto;
import com.ac.notification.system.domain.entity.Notification;
import com.ac.notification.system.domain.exception.EmailException;
import com.ac.notification.system.service.NotificationService;
import com.ac.notification.system.service.smtp.SmtpService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor_ = @Autowired)
public class EmailNotificationEngine implements NotificationService {
  
  private final SmtpService smtpService;
  
  @Override
  public NotificationDto send(final Notification notification) {
    notification.setEngine(NotificationEngine.EMAIL);
    try {
      smtpService.send(notification);
    } catch (MessagingException ex) {
      throw new EmailException("Unable to send email", ex);
    }
    return NotificationDto.fromDomainEntity(notification);
  }
  
  @Override
  public boolean supports(final NotificationEngine engine) {
    return engine == NotificationEngine.EMAIL;
  }
  
}
